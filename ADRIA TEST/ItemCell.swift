//
//  ItemCell.swift
//  ADRIA TEST
//
//  Created by macOs on 7/10/19.
//  Copyright © 2019 macOs. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(text: String) {
        self.textLabel.text = text
    }
}
