//
//  ViewController.swift
//  ADRIA TEST
//
//  Created by macOs on 7/9/19.
//  Copyright © 2019 macOs. All rights reserved.
//

import UIKit
import Firebase
class ViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var msgLabel: UILabel!
    var handle: AuthStateDidChangeListenerHandle?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkAuth()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onRegisterClicked(_ sender: Any) {
        
        let email:String? = emailField.text
        let password:String? = passwordField.text
        Auth.auth().createUser(withEmail: email!, password: password!) { user, error in
            if error == nil && user != nil {
                print("User created!")
                self.performSegue(withIdentifier: "authTOgrid", sender: self)
                
            } else {
                print("Error: \(error!.localizedDescription)")
                self.showToast(message: error!.localizedDescription)
            }
        }
    }
    
    
    @IBAction func OnLoginClicked(_ sender: Any) {
        let email:String? = emailField.text
        let password:String? = passwordField.text
        Auth.auth().signIn(withEmail: email!, password: password!) { user, error in
            if error == nil && user != nil {
                self.dismiss(animated: false, completion: nil)
                self.performSegue(withIdentifier: "authTOgrid", sender: self)
            } else {
                print("Error logging in: \(error!.localizedDescription)")
                self.showToast(message: error!.localizedDescription)
            }
        }
        
    }
    func setMessage(message:String){
        self.msgLabel.text = message
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        //toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    private func checkAuth() {
        // self.setupRootViewController(viewController: SplashViewController())
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil {//No body is authentified
                print("not registred")
                
            } else {
                let emailValue = user?.email
                print(emailValue)
                self.performSegue(withIdentifier: "authTOgrid", sender: self)
                
                //try! Auth.auth().signOut()
                
            }
        }
    }
    
}

